﻿#include <iostream>
#include <queue>
#include <thread>
#include <chrono>
#include <mutex>
#include "windows.h"

using namespace std;

mutex mtx;
mutex mtx_1;
mutex mtx_2;




class Task
{
public:
	Task(string name, string queue_name, int delay_value, int priority)
	{
		this->Task_name = name;
		this->Task_queue = queue_name;
		this->Task_delay = delay_value;
		this->Task_priority = priority;
	}

	string Get_Task_name() { return Task_name; }
	string Get_task_queue() { return Task_queue; }
	int Get_delay() { return Task_delay; }
	int Get_priority() { return Task_priority; }


public:
	string Task_name, Task_queue;
	int Task_delay, Task_priority;
};

std::ostream& operator << (std::ostream& os, const Task& p)
{
	return os << p.Task_name;
}

class GreaterPriority
{
public:
	bool operator()(Task& Task1_S, Task& Task2_S)
	{
		return Task1_S.Get_priority() > Task2_S.Get_priority();
	}
};


priority_queue<Task, vector<Task>, GreaterPriority>  queueS;



void Task_S_completion(queue<Task> queueD, Task Task_S, Task Task_D)
{
	char buffer[80];
	time_t seconds = time(NULL);
	tm* timeinfo = localtime(&seconds);
	const char* format = "%a %b %d %Y %I:%M:%S";
	strftime(buffer, 80, format, timeinfo);

	while(!queueS.empty())
	{
		mtx_2.lock();
		strftime(buffer, 80, format, timeinfo);
		cout << buffer << " : " << Task_S.Get_task_queue() << " : " << queueS.top() << " - " << Task_S.Get_delay() << " : running..." << endl;
		mtx_2.unlock();
		mtx.lock();
		this_thread::sleep_for(chrono::seconds(Task_S.Get_delay()));
		mtx.unlock
		time_t seconds_2 = time(NULL);
		tm* timeinfo_2 = localtime(&seconds_2);
		strftime(buffer, 80, format, timeinfo_2);
		mtx_2.lock();
		cout << buffer << " : " << Task_S.Get_task_queue() << " : " << queueS.top() << " - " << Task_S.Get_delay() << " complited." << endl;
		queueS.pop();
		queueD.push(Task_D);
		mtx_2.unlock();
	}
}

void Task1_D_completion(queue<Task> queueD1)
{
	char buffer[80];
	time_t seconds = time(NULL);
	tm* timeinfo = localtime(&seconds);
	const char* format = "%a %b %d %Y %I:%M:%S";

	Task Task1_D("taskD1", "queueD1", 10, 0);
	if (queueD1.empty())
	{
		queueD1.push(Task1_D);
	}
	while (!queueD1.empty())
	{


		strftime(buffer, 80, format, timeinfo);
		mtx_1.lock();
		cout << buffer << " : " << Task1_D.Get_Task_name() << " - " << Task1_D.Get_delay() << " : " << "created." << endl;
		mtx_1.unlock();
		this_thread::sleep_for(chrono::seconds(Task1_D.Get_delay()));
		time_t seconds_2 = time(NULL);
		tm* timeinfo_2 = localtime(&seconds_2);
		strftime(buffer, 80, format, timeinfo_2);
		Task Task1_S("taskS1", "queueS", 2, 1);
		queueS.push(Task1_S);
		mtx_1.lock();
		cout << buffer << " : " << Task1_D.Get_Task_name() << " - " << Task1_D.Get_delay() << " : " << "( " << Task1_S.Get_Task_name() << " : " << Task1_S.Get_task_queue() << " )" << " pushed." << endl;
		Task_S_completion(queueD1, Task1_S, Task1_D);
		mtx_1.unlock();
	}
};

void Task2_D_completion(queue<Task> queueD2)
{
	char buffer[80];
	time_t seconds = time(NULL);
	tm* timeinfo = localtime(&seconds);
	const char* format = "%a %b %d %Y %I:%M:%S";

	Task Task2_D("taskD2", "queueD2", 10, 0);
	if (queueD2.empty())
	{
		queueD2.push(Task2_D);
	}

	while (!queueD2.empty())
	{


		strftime(buffer, 80, format, timeinfo);
		mtx_1.lock();
		cout << buffer << " : " << Task2_D.Get_Task_name() << " - " << Task2_D.Get_delay() << " : " << "created." << endl;
		mtx_1.unlock();
		this_thread::sleep_for(chrono::seconds(Task2_D.Get_delay()));
		time_t seconds_2 = time(NULL);
		tm* timeinfo_2 = localtime(&seconds_2);
		strftime(buffer, 80, format, timeinfo_2);
		Task Task2_S("taskS2", "queueS", 2, 2);
		queueS.push(Task2_S);
		mtx_1.lock();
		cout << buffer << " : " << Task2_D.Get_Task_name() << " - " << Task2_D.Get_delay() << " : " << "( " << Task2_S.Get_Task_name() << " : " << Task2_S.Get_task_queue() << " )" << " pushed." << endl;
		Task_S_completion(queueD2, Task2_S, Task2_D);
		mtx_1.unlock();
	}
};

void Task3_D_completion(queue<Task> queueD3)
{
	char buffer[80];
	time_t seconds = time(NULL);
	tm* timeinfo = localtime(&seconds);
	const char* format = "%a %b %d %Y %I:%M:%S";
	strftime(buffer, 80, format, timeinfo);
	Task Task3_D("taskD3", "queueD3", 10, 0);
	if (queueD3.empty())
	{
		queueD3.push(Task3_D);
	}
	while (!queueD3.empty())
	{


		strftime(buffer, 80, format, timeinfo);
		mtx_1.lock();
		cout << buffer << " : " << Task3_D.Get_Task_name() << " - " << Task3_D.Get_delay() << " : " << "created." << endl;
		mtx_1.unlock();
		this_thread::sleep_for(chrono::seconds(Task3_D.Get_delay()));
		time_t seconds_2 = time(NULL);
		tm* timeinfo_2 = localtime(&seconds_2);
		strftime(buffer, 80, format, timeinfo_2);
		Task Task3_S("taskS3", "queueS", 2, 3);
		queueS.push(Task3_S);
		mtx_1.lock();
		cout << buffer << " : " << Task3_D.Get_Task_name() << " - " << Task3_D.Get_delay() << " : " << "( " << Task3_S.Get_Task_name() << " : " << Task3_S.Get_task_queue() << " )" << " pushed." << endl;
		Task_S_completion(queueD3, Task3_S, Task3_D);
		mtx_1.unlock();
	}
};

int main()
{
	queue <Task> queueD1;
	queue <Task> queueD2;
	queue <Task> queueD3;

	while (queueD1.empty() || queueD2.empty() || queueD3.empty())
	{
		thread th1_D(Task1_D_completion, queueD1);
		thread th2_D(Task2_D_completion, queueD2);
		thread th3_D(Task3_D_completion, queueD3);

		th1_D.join();
		th2_D.join();
		th3_D.join();
	}
	return 0;
}